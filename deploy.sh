#!/bin/sh

. ~/src/bitbucket.org/${CI_REPO_NAME}/project.sh
cd ~/src/bitbucket.org/${CI_REPO_NAME}
git remote add wpe-${CI_BRANCH} git@git.wpengine.com:${CI_BRANCH}/${PROJECT_NAME}.git > /dev/null 2>&1
git fetch --unshallow
git push wpe-${CI_BRANCH} ${CI_COMMIT_ID}:refs/heads/master -f