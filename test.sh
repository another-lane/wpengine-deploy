#!/bin/bash
. ~/src/bitbucket.org/${CI_REPO_NAME}/project.sh
cd ~/src/bitbucket.org/${CI_REPO_NAME}/wp-content/themes/${THEME_NAME}
gulp build