#!/bin/bash

# Install PHP 5.5.9
#====================
phpenv local 5.5
# Install Node
#====================
gem install compass
gem install sass
npm rm --global gulp
npm install --global gulp-cli
npm install --save-dev gulp
npm install